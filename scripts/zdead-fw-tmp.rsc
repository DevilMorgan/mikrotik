/ip firewall filter
remove [find]
add chain=forward connection-state=invalid action=log log-prefix=FW-INV:
add chain=forward connection-state=invalid action=drop 
add chain=forward connection-state=established action=accept
add chain=forward connection-state=related action=accept
add chain=forward jump-target=protochecks protocol=tcp action=jump 
add chain=forward jump-target=protochecks protocol=udp action=jump 
add chain=forward disabled=yes in-interface=br-www action=drop 
add chain=forward action=jump jump-target=fwd-users in-interface=br-users 
add 	chain=fwd-users out-interface=br-www action=accept
add 	chain=fwd-users out-interface=br-mgmt action=accept
add action=jump chain=forward in-interface=br-media jump-target=fwd-media

add chain=input connection-state=invalid log-prefix=IN-INV action=log
add chain=input connection-state=invalid action=drop
add chain=input protocol=icmp jump-target=in-icmp action=jump 
add 	chain=in-icmp dst-limit=8,32,dst-address/15m
add 	chain=in-icmp action=reject dst-limit=60,512,dst-address/15m reject-with=icmp-host-prohibited
add 	chain=in-icmp action=add-src-to-address-list address-list=global-block address-list-timeout=1w 
add 	chain=in-icmp action=drop
add chain=input connection-state=established action=accept
add chain=input connection-state=related action=accept
add chain=input in-interface=br-mgmt
add action=drop chain=input connection-state=new in-interface=br-www
add chain=input dst-port=53 in-interface=!br-www protocol=udp
add chain=input dst-address=10.7.1.1 dst-port=22 protocol=tcp
add chain=input dst-address=10.7.1.1 dst-port=80 protocol=tcp
add chain=input dst-address=10.7.1.1 dst-port=443 protocol=tcp
add action=log chain=input disabled=yes log-prefix=INPUT-HUH

add chain=output connection-state=established
add chain=output connection-state=related
add chain=output dst-port=53 out-interface=br-www protocol=udp src-address-list=us
add chain=output dst-port=123 out-interface=br-www protocol=udp src-address-list=us src-port=123
add chain=output disabled=yes dst-address-list=znet-6rdend out-interface=br-www protocol=ipv6
add chain=output disabled=yes action=log connection-state=new log-prefix=OUT-NEW
add chain=output disabled=yes action=log 

add chain=protochecks comment="TCP flags and Port 0 attacks" dst-port=0 protocol=tcp action=drop
add chain=protochecks action=drop protocol=tcp src-port=0
add chain=protochecks action=drop protocol=tcp tcp-flags=fin,!ack
add chain=protochecks action=drop protocol=tcp tcp-flags=rst,urg
add chain=protochecks action=drop protocol=tcp tcp-flags=fin,urg
add chain=protochecks action=drop protocol=tcp tcp-flags=fin,syn
add chain=protochecks action=drop protocol=tcp tcp-flags=fin,rst
add chain=protochecks action=drop protocol=tcp tcp-flags=syn,rst
add chain=protochecks action=drop protocol=tcp tcp-flags=!fin,!syn,!rst,!ack
add chain=protochecks action=drop protocol=udp src-port=0
add chain=protochecks action=drop dst-port=0 protocol=udp
