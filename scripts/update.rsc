#Packages
/system package update
check-for-updates
:delay 5s
:if ( [get current-version] != [get latest-version]) do={ upgrade }

#Layer7 filters
/tool fetch url=http://www.mikrotik.com/download/l7-protos.rsc
/import l7-protos.rsc
/file remove l7-protos.rsc
