:local direction $1;
:if ($direction != "in" && $direction != "ex") do={
    :put "Specify 'in' or 'ex'";
    :return;
}
:if ($direction = "in") do={
    :set direction true;
} else={
    :set direction false;
}

:local check $2;
:if ($check != "key" && $check != "value" && $check != "both") do={
    :put "Specify 'key', 'value' or 'both'";
    :return
}
:local checkkeys false;
:if ($check = "key" || $check = "both") do={
    :set checkkeys true;
} 
:local checkvals false;
:if ($check = "value" || $check = "both") do={
    :set checkvals true;
} 

:local filters $3;
if ([:typeof $filters] != "array") do={
   :put $filters;
   :set filters { 0=$filters };
   :put "Moved filters to array";
   :put [:typeof $filters];
   :put $filters;
}

:local array $4;
if ([:typeof $array] != "array") do={
   :put "Feed me an array to array-filter";
   :return
}

:local match do={
#    :put "MatchRules:"
#    :put $2
    :foreach rule in=$2 do={
        :put "Matching $3 against $rule..."
        :if ($3 ~ $rule) do={
            :put "Matched! Returning $1..."
            :return $1;
        }
    }
}

:put "MATCHING:";
#:put $checkkeys;
#:put $checkvals;
:put $direction;
:put $filters;
:put $array;

:local result { 0 };
if ($checkkeys) do={
    :foreach k,v in=$array do={
        :put "Matching $k...";
        :if ([$match $direction $filters $k] = true) do={
            :set ($result->$k) $v;
        }
    }
    :set array [:pick $result 1 [:len $result]];
}

if ($checkvals) do={
    :foreach k,v in=$array do={
        :put "Matching $v...";
        :if ([$match $direction $filters $v] = true) do={
            :put "We have a match";
            :set ($result->$k) $v;
        }
    }
}

:set result [:pick $sorted 1 [:len $sorted]];

:put "RESULT:"
:put $result;
:return $result;