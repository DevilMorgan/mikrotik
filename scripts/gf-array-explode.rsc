:global toarray;
:local explode do={
    #:put "Exploding $1 along $2 axis...";
    :local input $1;
    :local sep $2;
    :local working true;
    :local exploded;

    :if ([:typeof $sep] = "num") do={
        :local length [:len $input];
        :local pos 0;
        :do { 
            :set exploded ($exploded , [:pick $input $pos ($pos + $sep)]);
            :set pos ($pos + $sep);
        } while=( $pos < $length );
    } else={
        :do { 
            :local split [:find $input $sep];
            if ([:len $split] = 0) do={
                :set working false;
                :set exploded ($exploded , $input);
            } else={
                :set exploded ($exploded , [:pick $input 0 $split]);
                :set input [:pick $input ($split + 1) [:len $input]];
            }
        } while=( $working );
    }
    :return $exploded;
}

:local input $1;
:local type [:typeof $input];
:local sep false;
if ([:len $2] != 0) do={
    :set sep $2;
#    :put "Arg2 $2 > $[:typeof $2] > sep $sep > $[:typeof $sep]";
}

if ($type = "ip" && $sep = false ) do={
    :set sep ".";
}
if ($type = "ip6" && $sep = false ) do={
    :set sep ":";
}

:local exploded;
if ($type = "array") do={
    foreach i in=$input do={
        :set exploded ($exploded , $toarray $i $sep);
    }
} else={
    :if ($sep = false) do={
       :set exploded $input;
    } else={
       :set exploded [$explode $input $sep];
    }
}

:return $exploded;
