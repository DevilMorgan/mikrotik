#remove an element $2 from an array $1
:local arr $1;
:local p $2;

if ([:typeof $p] = "num") do={
    :set arr ([:pick $arr 0 $p],[:pick $arr ($p+1) [:len $arr]]);
} else={
    :set ($arr->$p);
}
:return $arr;