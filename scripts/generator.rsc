#   v6pool="ZweetlandPret";
:local settings {
   v4net="10.7.0.0/24";
   oui="DE:0F:11:05:0";
   wan="br666-www";
}

:global toarray

:local macprefix ($settings->"oui")

:local v4prefix ($settings->"v4net")
:set v4prefix [$toarray $v4prefix "."]
:set v4prefix "$[:pick $v4prefix 0].$[:pick $v4prefix 1]."

#:local v6prefix [/ipv6 pool get [/ipv6 pool find name=($settings->"v6pool")] prefix]
:local v6prefix [/ipv6 dhcp-client get [find interface=($settings->"wan")] prefix]
:set v6prefix [$toarray $v6prefix ":"]
:set v6prefix "$[:pick $v6prefix 0]:$[:pick $v6prefix 1]:$[:pick $v6prefix 2]:"

:put "Settings now $v4prefix / $v6prefix"

:foreach vlanBridge in=[/interface bridge find name~"^br[0-9]{3}"] do={
    :local bridgeName [/interface bridge get $vlanBridge name]
    :local vlanID [:pick $bridgeName 2 5]
    :local vlanNet ($vlanID / 10)

    :if ((1000 + $vlanID) != (1000 + $vlanNet * 10)) do={
       :put "$bridgeName doesn't adhere to standard, skipping..."
#       :put "$(1000+$vlanID) isnt $(1000 + $vlanNet * 10)"
#       :put "$vlanID isnt $vlanNet"
    } else={

       :local vlanName [:pick $bridgeName 6 [:len $bridgeName]]
       :local slaveName "$vlanID$vlanName"
       :local addrName "if$vlanID-$vlanName" 
       :local netName "net$vlanID-$vlanName" 

       :local mac "$macprefix$[:pick $vlanID 0 1]:$[:pick $vlanID 1 3]"

       :local ip4addr "$v4prefix$vlanNet.1" 
       :local ip4net "$v4prefix$vlanNet.0/24"

       :local ip6addr "$v6prefix$vlanNet::1"
       :local ip6net "$v6prefix$vlanNet::0/64"

       :put "Bridge $bridgeName gets MAC $mac..."

       :foreach slave in=[/interface find name~"-$slaveName\$"] do={
          :put "Adding $[/interface get $slave name] to $bridgeName..."
       }

       :put "Adding $addrName as $ip4addr to FW addr list..."
       :put "Adding $netName as $ip4net to FW addr list..."

       :put "Adding $addrName as $ip6addr to FW addr list..."
       :put "Adding $netName as $ip6net to FW addr list..."

       :put "$vlanName in $ip4net"
    }
}