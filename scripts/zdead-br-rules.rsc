/interface bridge filter
remove [find]
#add action=jump chain=input disabled=yes in-bridge=br-users jump-target=br-users
#add action=drop chain=forward comment="PVLAN mode\?" in-bridge=br-guests
#add disabled=yes action=log chain=users log-prefix=BRSTUFFIE! out-bridge=br-www

add action=jump chain=input disabled=yes in-bridge=br-users jump-target=br-users
add action=drop chain=forward comment="PVLAN mode\?" in-bridge=br-guests
add chain=input disabled=yes
add chain=output disabled=yes
add action=log chain=users disabled=yes log-prefix=BRSTUFFIE! out-bridge=br-www
add chain=input comment="Accept IPv6 traffic" in-bridge=br-www mac-protocol=ipv6
add chain=input comment="Accept IP traffic to me" dst-address=82.176.206.154/32 mac-protocol=ip
add chain=input comment="Accept good ARP" dst-mac-address=DE:0F:11:05:0F:1E/FF:FF:FF:FF:FF:FF in-bridge=br-www mac-protocol=arp
add action=drop arp-dst-address=!82.176.206.154/32 arp-opcode=request chain=input comment="Stupid ZeelandNet ARP requests" in-bridge=br-www mac-protocol=arp
add chain=input comment="Accept ARP\?" disabled=yes in-bridge=br-www mac-protocol=arp packet-type=broadcast
add chain=input comment="Accept DHCP" dst-port=68 in-bridge=br-www ip-protocol=udp mac-protocol=ip packet-type=broadcast src-port=67
add action=log chain=input comment="Log Traffic that's 'odd'" in-bridge=br-www log-prefix=BR-STR
add chain=output comment="Accept from my IP" mac-protocol=ip out-bridge=br-www src-address=82.176.206.154/32
add chain=output comment="Accept Host IPv6 (WTF MikroTik!\?)" mac-protocol=ipv6 out-bridge=br-www packet-type=host
add chain=output comment="ARP is acceptable" mac-protocol=arp out-bridge=br-www
add chain=output comment="STP, LLDP, Ethernet stuff" dst-mac-address=01:80:C2:00:00:00/FF:FF:FF:FF:FF:00 mac-protocol=0x27
add chain=output mac-protocol=0x27
add action=log chain=output log-prefix=OUTARP-TF out-bridge=br-www
add action=log chain=input dst-mac-address=01:80:C2:00:00:00/FF:FF:FF:FF:FF:FF log-prefix=STP stp-type=config
