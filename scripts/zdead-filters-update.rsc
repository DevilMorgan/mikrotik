###VARIABLES
:global filterBridge;
:global filterIPv4;
:global filterIPv6;
:global filters;

:local IPv4filter {
   name = "IPv4 filter";
   location = "/ip firewall filter";
   rules = "filter-rules-ip";
   store = "ipv4filter";
   hash = $IPv4filter->rules;

}

###FUNCTIONS
#Check the hash of script $1 against $2
#return hash of script if the two aren't equal
#or revert if they are AND there's still a backup
:local WhatToDoWith do={
    :global md5sum;
    :local hash [$md5sum [/system script get $1 source]];
    :put "Comparing $hash of $1 with $2"
    :if ($hash != $2) do={
        :put "$hash is not $2!"
        :return $hash
    } else={
        :if ([:len [/file find name=("previous-" . $1 . ".rsc")]] > 0) do={
            :put "I'm going to revert $1...";
            :return "revert";
        } else={
            :return ""
        }
    }
}

:local update do={
:local rules;
if ($action = "revert") do={
    :put "$name: Reverting to previous rules";
#    :set rules [:parse "/import 
}


}

###EXECUTION
#:local updateBridge [$modify filter-rules-bridges $filterBridge];
:local updateIPv4 [$WhatToDoWith filter-rules-ip $filterIPv4];
#:local updateIPv6 [$modify filter-rules-ip $filterIPv6];

#:put "YOYO $updateIPv4"

:if ([:len $updateIPv4] > 0) do={
    :local rules;
    :if ($updateIPv4 = "revert") do={
        :put "IPv4: Reverting to previous rules;"
        :set rules [:parse "/import previous-filter-rules-ip.rsc"];
        /system script environment remove filterIPv4;
    } else={
        :put "IPv4: Creating backup of previous rules;"
        /ip firewall filter export file=previous-filter-rules-ip;
        :set rules [:parse "/ip firewall filter;$[ /system script get filter-rules-ip source ]"];
        :global filterIPv4 $updateIPv4;
    }
    /ip firewall filter;
    :put "IPv4: Marking old rules...";
    set [find] comment=TOBEDEVOURED;
    :put "IPv4: Adding new rulez...";
    $rules;
    :put "IPv4: Adding temporary DROP rules..."
#For some reason running this command multiple times requires this:
#:local bull [print]
#Apparently the item numbering doesn't reset between runs or something.
#Either figure out how to reset them or figure out how to print to /dev/null
    add chain=input action=drop place-before=0 comment="UPDATING"
    add chain=forward action=drop place-before=0 comment="UPDATING"
    add chain=output action=drop place-before=0 comment="UPDATING"

    :put "IPv4: Removing old rules..."
    remove [find comment=TOBEDEVOURED]
    :put "IPv4: Removing temporary DROP rules..."
    remove [find comment=UPDATING]
} else={
    :put "IPv4: Fine. Just fine."
}
