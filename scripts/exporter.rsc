:local config {
    start={ "chain";"disabled";"dst-port";"src-port" };
    end={ "action" };
}

:local del do={
    #remove an element from an array
    :local arr $1;
    :local p $2;

    if ([:typeof $p] = "num") do={
        :set arr ([:pick $arr 0 $p],[:pick $arr ($p+1) [:len $arr]]);
    } else={
        :set ($arr->$p);
    }
    :return $arr;
}



:local ararr {""};

#:put ($config->"start");


:local location [:parse "/interface bridge filter"];
$location;
print as-value;
:foreach i,rule in=[print as-value] do={
    :put "For $i";

    #If you're going to run an array with named items in it, 
    #you apparently better reinitialise it with a variable item in it.
    #Else all the old values will be retained, at least in 6.15
    :local okeys { 0 }
    :local ovals { 0 };
    #:set rule [$del $rule ".id"];
    
    :foreach k in=($config->"start") do={
        :local v [get $i $k];
#        :local v ($rule->$k);
        :if ([:len $v] > 0) do={
            #:put "Adding $k ($v) as keys";
            :set okeys ($okeys, $k);
            :set ovals ($ovals, $v);
            :set rule [$del $rule $k];
        } 
    }
    :set okeys [:pick $okeys 1 [:len $okeys]];
    :set ovals [:pick $ovals 1 [:len $ovals]];

#    :put "ORDER NOW:"
#    :put $okeys;
#    :put $ovals;

#    :local testout; 
#    :foreach k,v in=$ordered do={
#        :set testout ($testout . " $k=$v");
#    }
#    :put $testout;

    :local endkeys { 0 };
    :local endvals { 0 };
    :foreach k in=($config->"end") do={
        :local v ($rule->$k);
        :if ([:len $v] > 0) do={
            #:put "Adding $k ($v) to endkeys";
            :set endkeys ($endkeys, $k);
            :set endvals ($endvals, $v);
            :set rule [$del $rule $k];
        }
    }
    :set endkeys [:pick $endkeys 1 [:len $endkeys]];
    :set endvals [:pick $endvals 1 [:len $endvals]];

    #:put "END NOW:"
    #:put $endkeys;
    #:put $endvals;


    :foreach k,v in=$rule do={
        :if ([:len $v] > 0) do={
            #:put "Adding $k ($v) to ordered";
            :set okeys ($okeys, $k);
            :set ovals ($ovals, $v);
            :set rule [$del $rule $k];
        }
    }
  
    #:put "Merging $ordered with $endkeys";
    #:local testout; 
    #:foreach k,v in=$ordered do={
    #:set testout ($testout . " $k=$v");
    #}
    #:put $testout;

    :set okeys ($okeys , $endkeys);
    :set ovals ($ovals , $endvals);

    #:put "AFTER MERGE:";
    #:put $okeys;
    #:put $ovals;

    :local out; 
    :for i from=0 to=([:len $okeys]-1) step=1 do={
        :local okey [:pick $okeys $i];
        :local oval [:pick $ovals $i];
        :if ( [:typeof $oval]="str" ) do={
            :set oval ("\"" . $oval . "\"");
        }
        :set out ($out . " $okey=$oval");
    };

    #:foreach k,v in=$ordered do={
    #}
    :put $out;
    #:set ararr ($ararr , $arr);
    #:put $arr;
}

#:put $ararr;

#:local allout;
#:foreach r in=$ararr do={
#    :local out "add";
#    :foreach k,v in=$r do={
#        :set out ($out . " $k=$v");
#    }
#    :put $out;
#}