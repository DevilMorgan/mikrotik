### Ruleset updater
### So I can make nicely formatted rules
### And revert them if necessary/automatically
### Or commit them
###
### REQUISITES:
### * :global time - function for giving some datetime after offset
### * :global fnv - FNV-1a hash function
###

###VARIABLES
:global rulehash {
    ipv4filter="hoi";
#     ipv4filter="cd5b349de2adac4056be08f656b08aa2";
};

#Time to Revert
:local ttr "15m";

:local rulesets {
    ipv4filter={
        title="IPv4 filter";
        location="/ip firewall filter";
        rules="rules-ip-firewall";
    };

#    ipv6filter={
#        title="IPv6 filter";
#        location="/ipv6 firewall filter";
#        rules="rules-ip-firewall";
#    }

#    brfilter={
#        title="Bridge filter";
#        location="/interface bridge filter";
#        rules="rules-bridge-filter";
#    }
}

###FUNCTIONS
:local save do={
    :global part;
    /system scheduler remove [find name="REVERTER"];
    /file remove [/file find name ~ "revert-.*\\.rsc"];
    $part save;
}

:local update do={
    #Take in Globals
    :global rulehash;
    :global fnv;

    #Parse the object
    :local title ($config->"title");
    :local rules ($config->"rules");
    :local location ($config->"location");

    #Deduction, my dear Watson.
    :local ttr ("+" . $ttr);
    :local backup ("revert-" . $rules . ".rsc");
    :local store $name;
    :local last ($rulehash->$store);

    #Be awesome-o-paranoid with filters; 
    #add DROP rules before activating the new ones
    :local filter false;
    :if ($location = "/interface bridge nat" || $location ~ ".* filter") do={
        :set filter true;
    } 

    :local hash;
    :if ([:len [/system script find name=$rules]] > 0) do={
        #This crashes the console if $rules doesn't exist
        :set hash [$fnv [/system script get $rules source]];
    } else={
        :put "$title: WARNING - $rules doesn't exist";
    }

    #GoGo Gadget Updater!
    #The executor var will be continuously filled with :parse commands
    #Why? 
    #1) Because that's the only way :parse seems to do anything
    #2) Because the menu context is isolated to a :parse
    :local executor;
    :local ruleset;
    :local tostore;

    :put "$title: comparing $hash with $last";
    :if ($action = "revert" ||\
        ($hash = $last && $action != "force-update") ||\
        [:len $hash] = 0) do={
        :if ([:len [/file find name=$backup]] > 0) do={
            :put "$title: Reverting...";
            :set tostore;
            :set ruleset [:parse "/import $backup"];
        } else={
            :put "$title: Fine, fine, just fine";
            :return "";
        }
    } else={
        $save;
        :set tostore $hash;
        :put "$title: Updating, creating backup...";
        :set executor [:parse "$location export file=$backup"];
        $executor;
        #prep new ruleset;
        :set ruleset [:parse "$location ; $[/system script get $rules source]"];
        #Scheduling revert
        :global time;
        :local schedule [$time $ttc];
        :local task [/system scheduler find name="REVERTER"];
        :if ([:len $task] = 0) do={
            #do actually something/policy? ;) 
            :put "Scheduling at start-date=$[:pick $schedule 0] start-time=$[:pick $schedule 1]";
            /system scheduler add name="REVERTER" \
                start-date=[:pick $schedule 0] start-time=[:pick $schedule 1] \
                policy=read,write interval=0 on-event=gf-rulesets \
                comment="Processing Rulesets scheduled revert";
        } else={
            /system scheduler set $task start-date=[:pick $schedule 0] start-time=[:pick $schedule 1];
        }
    }

    #Do something useful
    :put "$title: Marking old rules...";
    :set executor [:parse "$location set [find] comment=DEVOURE"];
    $executor;
    :put "$title: Adding new rules...";
    $ruleset;
    :if ($filter) do={
        :put "$title: DROP all while updating rules...";
        #Running this multiple times apparently fails to reset the count or something,
        #So we do a print to force it...
        :set executor [:parse "$location ; \
            print as-value; \
            add chain=input action=drop place-before=0 comment=UPDATING; \
            add chain=forward action=drop place-before=0 comment=UPDATING; \
            add chain=output action=drop place-before=0 comment=UPDATING; "];
        $executor;
        :put "$title: Removing old rules...";
        :set executor [:parse "$location remove [find comment=DEVOURE]"];
        $executor;
        :put "$title: Stop DROPping all...";
        :set executor [:parse "$location remove [find comment=UPDATING]"];
        $executor;
    } else={
        :put "$title: Removing old rules...";
        :set executor [:parse "$location remove [find comment=DEVOURE]"];
        $executor;
    }

    #Update global
    :set ($rulehash->$store) $tostore;

}

###EXECUTION
:local action;

:if ([:len $1] > 0) do={
    :set action $1;
} else={
    :if ([:len [/file find name ~ "revert-.*\\.rsc"]] > 0) do={
        :set action "revert";
    }
}

:if ($action = "save") do={
    $save;
} else={
    :foreach name,config in=$rulesets do={ 
        $update save=$save ttr=$ttr name=$name config=$config action=$action; 
    }
#    :if ($action = "revert") do={
#        $save;
#    }
}
