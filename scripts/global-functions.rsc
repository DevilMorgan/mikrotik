:local action;

:if ([:len $1] > 0) do={
    :set action $1;
} else={
    :global gfhash;
    :if ([:len $gfhash] = 0) do={
        :set action "install";
    } else={
        #Reinstall upon update just to be sure
        if ( [/system package update get current-version] != ($gfhash->"rosver") ) do={
           :set action "install";
        } else={
           :set action "update";
        }
    }
}

####FUNCTIONS
:local install do={
    /system scheduler remove [find name="Load Global Functions"];
    /system scheduler add start-time=startup name="Load Global Functions" interval=0 on-event=global-functions policy=read,write comment="Load & update all global functions";

    :global gfhash { rosver=[/system package update get current-version] };
    :global fnv [:parse [/system script get gf-hash-fnv-1a source]];

    :global ascii { "\00"; "\01"; "\02"; "\03"; "\04"; "\05"; "\06"; "\07"; \
    "\08"; "\09"; "\0A"; "\0B"; "\0C"; "\0D"; "\0E"; "\0F"; \
    "\10"; "\11"; "\12"; "\13"; "\14"; "\15"; "\16"; "\17"; \
    "\18"; "\19"; "\1A"; "\1B"; "\1C"; "\1D"; "\1E"; "\1F"; \
    "\20"; "\21"; "\22"; "\23"; "\24"; "\25"; "\26"; "\27"; \
    "\28"; "\29"; "\2A"; "\2B"; "\2C"; "\2D"; "\2E"; "\2F"; \
    "\30"; "\31"; "\32"; "\33"; "\34"; "\35"; "\36"; "\37"; \
    "\38"; "\39"; "\3A"; "\3B"; "\3C"; "\3D"; "\3E"; "\3F"; \
    "\40"; "\41"; "\42"; "\43"; "\44"; "\45"; "\46"; "\47"; \
    "\48"; "\49"; "\4A"; "\4B"; "\4C"; "\4D"; "\4E"; "\4F"; \
    "\50"; "\51"; "\52"; "\53"; "\54"; "\55"; "\56"; "\57"; \
    "\58"; "\59"; "\5A"; "\5B"; "\5C"; "\5D"; "\5E"; "\5F"; \
    "\60"; "\61"; "\62"; "\63"; "\64"; "\65"; "\66"; "\67"; \
    "\68"; "\69"; "\6A"; "\6B"; "\6C"; "\6D"; "\6E"; "\6F"; \
    "\70"; "\71"; "\72"; "\73"; "\74"; "\75"; "\76"; "\77"; \
    "\78"; "\79"; "\7A"; "\7B"; "\7C"; "\7D"; "\7E"; "\7F" };
}

#Insert/Update/$1 a global function $2 from script $3
:local insertGF do={
    :global fnv;
    :global gfhash;

    :local action $1;
    :local name $2;
    :local script $3; 
    :local source [/system script get $script source]; 
    :local hash;

    :if ($action = "update" || $action = $script) do={
        :set hash [$fnv $source];
    }
    #:put "Checking $name $script ($hash)";

    :local exec;
    :if ([:len $hash] = 0 || $hash = ($gfhash->$script)) do={
        :put "$name: Loading...";
        :set exec [:parse ":global $name"];
    } else={
        :put "$name: Updating with source from $script...";
        :set exec [:parse ":global $name [:parse [/system script get $script source]]"];
        :set ($gfhash->$script) $hash;    
    }
    $exec;
}

###EXECUTION
:if ($action = "install") do={
    $install;
    :set action "update";
} 

#Generic tools
$insertGF $action "fnv" "gf-hash-fnv-1a";
$insertGF $action "gf" "global-functions";
:global gfl do={
    :global gf;
    :if ([:len $1] > 0) do={ 
         $gf $1;
    } else={ 
         $gf "load";
    } 
}
$insertGF $action "part" "gf-partitions";
#This one was sloooooow
#$insertGF $action "sort" "gf-quicksort";

#Array utils
$insertGF $action "sort" "gf-array-sort";
$insertGF $action "toarray" "gf-array-explode";
$insertGF $action "arf" "gf-array-filter";
$insertGF $action "ardl" "gf-array-delete";

#Date/time stuff
$insertGF $action "time" "gf-time";
$insertGF $action "wday" "gf-weekday";

#process rulesets
$insertGF $action "pr" "gf-rulesets";
:global commit do={ $pr "save" };
:global revert do={ $pr "revert" };
