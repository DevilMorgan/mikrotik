# :returns { daynumber, dayname }
# Calculates day of the week for a (MikroTik-syntax) given date
#
# by melboyscout (melboyscout [at] gmail.com)
# sligthly modified by R. van Zantvoort (van.zantvoort [at] gmail.com)
#
# See also:
# http://en.wikipedia.org/wiki/Calculating_the_day_of_the_week
# http://wiki.mikrotik.com/wiki/Script_to_find_the_day_of_the_week
# http://forum.mikrotik.com/viewtopic.php?f=2&t=59029

:local date;
:if ([:len $1] > 0) do={
    :set date $1;
} else={
    :set date [/system clock get date];
}

# Math Calculation here
:local result "";
:local months { "jan";"feb";"mar";"apr";"may";"jun";"jul";"aug";"sep";"oct";"nov";"dec" };
:local days { "sun";"mon";"tue";"wed";"thu";"fri";"sat" };

:local day [:tonum [:pick $date 4 6]];
:local month ([:find $months [:pick $date 0 3]] + 1);
:local year [:tonum [:pick $date 7 11]];

:local sum 0;
:local aaa 0;
:local yyy 0;
:local mmm 0;

:set aaa ((14 - $month) / 12);
:set yyy ($year - $aaa);
:set mmm ($month + 12 * $aaa - 2);
:set sum (7000 + $day + $yyy + ($yyy / 4) - ($yyy / 100) + ($yyy / 400) + ((31 * $mmm) / 12));
:set sum ($sum - (($sum / 7) * 7));
:set result [:pick $days $sum];
#:put "$date is day $sum ($result)";
:return { $sum ; $result };