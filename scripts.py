#!/usr/bin/python2
# -*- coding: utf-8 -*-

# import mt.rosapi
import os
import routeros_api
from data import mikrotiks

import pprint
pp = pprint.PrettyPrinter(indent=2)

path = './scripts/'


def get_device(name):
    mt = mikrotiks[name]
    return routeros_api.connect(mt['host'], mt['user'], mt['passwd'])


def upload_script(device, name):
    scripts = device.get_binary_resource('/system/script')

    fname = os.path.join(path, '{0}.rsc'.format(name))
    with open(fname) as rfh:
        src = rfh.read()

    scripts.add(name=name, source=src)


def download_scripts(device):
    scripts = device.get_binary_resource('/system/script').get()

    for script in scripts:
        fname = os.path.join(path, '{0}.rsc'.format(script['name']))
        contents = script['source']
        with open(fname, 'w') as tfh:
            tfh.write(contents)


if __name__ == '__main__':
    device = get_device('ronald')
    download_scripts(device)

    name = 'test-upl'
