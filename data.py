__author__ = 'theloeki'

mikrotiks = {
    'ronald': {
        'host': '2a02:f6e:a000:1::1',
        'user': 'admin',
        'passwd': 'example'
    },

    # No conntrack
    'fw01': {
        'host': 'firewall01.network.example.nl',
        'user': 'admin',
        'passwd': 'pass'
    },
    'fw02': {
        'host': 'firewall02.network.example.nl',
        'user': 'admin',
        'passwd': 'rpass'
    },

    # Conntracks
    'fw03': {
        'host': 'firewall03.network.example.nl',
        'user': 'admin',
        'passwd': 'rF86pass'
    },
    'fw04': {
        'host': 'firewall04.network.example.nl',
        'user': 'admin',
        'passwd': 'rF86FvWpass'
    },

}
